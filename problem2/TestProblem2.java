package problem2;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class TestProblem2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan=new Scanner(System.in);
		TreeSet<Integer> numbers=new TreeSet<Integer>();
		String answer="Y";
		while(true){
			System.out.println("Introduce numbers followed by Enter: ");
			numbers.add(scan.nextInt());
			System.out.println("Continue? Y/N");
			answer=scan.next();
			if(answer.equals("N")||answer.equals("n"))
				break;
		}
		System.out.println("the numbers are: ");
		for(Integer i:numbers)
			System.out.println(i);
		System.out.println("The lowest number is: "+numbers.first());
		System.out.println("The highest number is: "+numbers.last());
		System.out.println("The prime numbers are: ");
		for(Integer i:numbers)
			if(isPrime(i)==true)
				System.out.print(i);
		TreeSet<String> palindromes=new TreeSet<String>();
		for(Integer i:numbers)
			palindromes.add(i.toString());
		System.out.println("The palindromes are: ");
		for(String s:palindromes)
			if(isPalindrom(s)==true)
			 System.out.print(s);
			
		

	}
	public static boolean isPrime(Integer n){
		boolean isPrime=true;
		if(n<2)
			isPrime=false;
		for(int i=2;i<n;i++){
			if(n%i==0){
				isPrime=false;
				break;	
			}
		}
		return isPrime;
	}
	public static boolean isPalindrom(String s){
		boolean isPalindrom=true;
		int first=0;
		int last=s.length()-1;
		while(first<last){
			if(s.charAt(first)!=s.charAt(last)){
				isPalindrom=false;
				break;					
			}
			first++;
			last--;
		}
		return isPalindrom;
	}
}
