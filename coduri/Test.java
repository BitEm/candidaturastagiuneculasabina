package coduri;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Choose between circle (C), triangle (T) and rectangle(R): ");
		Scanner scan=new Scanner(System.in);
		String input=scan.next();
		if(input.equals("T")){			
			System.out.println("The length of the first side is: ");
			Double s1=scan.nextDouble();
			System.out.println("The length of the corresponding height is: ");
			Double h=scan.nextDouble();
			System.out.println("The length of the second side is: ");
			Double s2=scan.nextDouble();
			System.out.println("The length of the third side is: ");
			Double s3=scan.nextDouble();
			Triangle t=new Triangle(s1,s2,s3,h);
			System.out.println("The perimeter of the triangle is: "+t.getPerimeter());
			System.out.println("The area of the traingle is: "+t.getArea());
		}
		if(input.equals("R")){
			System.out.println("The first side of the rectangle is: ");
			Double s1=scan.nextDouble();
			System.out.println("The second side of the rectangle is: ");
			Double s2=scan.nextDouble();
			Rectangle r=new Rectangle(s1,s2);
			System.out.println("The perimeter of the rectangle is: "+r.getPerimeter());
			System.out.println("The rectangle's square is: "+r.getArea());
		}
		if(input.equals("C")){
			System.out.println("The radius of the circle is: ");
			Double r=scan.nextDouble();
			Circle c=new Circle(r);
			System.out.println("The circle's perimeter is: "+c.getPerimeter());
			System.out.println("The circle's square is: "+c.getArea());
		}		
		

	}


}
