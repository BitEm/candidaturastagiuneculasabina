package coduri;

public class Circle extends GeometricShape {
	private Double radius;

	public Double getRadius() {
		return radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}
	@Override
	public Double getPerimeter(){	
		if(this.radius>=0.0)
			this.perimeter=2*Math.PI*this.radius;
		return this.perimeter;
	}
	@Override
	public Double getArea(){
		if(this.radius>=0.0)
			this.area=Math.PI*Math.pow(this.radius, 2);
		return this.perimeter;
	}

	public Circle(Double radius) {
		super();
		this.radius = radius;
	}

	public Circle() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	
	

}
