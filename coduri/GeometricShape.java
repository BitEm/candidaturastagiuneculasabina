package coduri;

public abstract class GeometricShape {
	protected Double perimeter;
	protected Double area;
	public abstract Double getPerimeter();
	public abstract Double getArea() ;
	public GeometricShape() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
