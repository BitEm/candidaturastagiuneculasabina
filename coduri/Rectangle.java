package coduri;

public class Rectangle extends GeometricShape {
	private Double side1;
	private Double side2;	
	
	public Double getSide1() {
		return side1;
	}
	public void setSide1(Double side1) {
		this.side1 = side1;
	}
	public Double getSide2() {
		return side2;
	}
	public void setSide2(Double side2) {
		this.side2 = side2;
	}
	@Override
	public Double getPerimeter(){
		if(this.side1>=0.0&&this.side2>=0.0)
			this.perimeter=2*(this.side1+this.side2);
		return this.perimeter;
	}
	@Override
	public Double getArea(){
		if(this.side1>=0.0&&this.side2>=0.0)
			this.area=this.side1*this.side2;
		return this.area;
	}
	public Rectangle(Double side1, Double side2) {
		super();
		this.side1 = side1;
		this.side2 = side2;
	}
	public Rectangle() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
