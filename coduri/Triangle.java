package coduri;

public class Triangle extends GeometricShape {
	private Double side1;
	private Double side2;
	private Double side3;
	private Double height;
	public Double getSide1() {
		return side1;
	}
	public void setSide1(Double side) {
		this.side1 = side;
	}
	
	public Double getSide2() {
		return side2;
	}
	public void setSide2(Double side2) {
		this.side2 = side2;
	}
	public Double getSide3() {
		return side3;
	}
	public void setSide3(Double side3) {
		this.side3 = side3;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	@Override
	public Double getPerimeter(){
		if(this.side1>=0.0&&this.side2>=0.0&&this.side3>=0.0)
			this.perimeter=this.side1+this.side2+this.side3;
		return this.perimeter;
	}
	@Override
	public Double getArea() {
		// TODO Auto-generated method stub
		if(this.side1>=0.0&&this.height>=0.0)
			this.area=(this.side1*this.height)/2;
		return this.area;
	}
	public Triangle(Double side1, Double side2, Double side3, Double height) {
		super();
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
		this.height = height;
	}
	public Triangle() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
